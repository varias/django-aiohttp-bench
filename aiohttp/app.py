import asyncio
from datetime import datetime

from aiohttp.web import Application, View, Response
import asyncpg
from envparse import Env

env = Env(
    POSTGRES_DB=dict(cast=str, default='application'),
    POSTGRES_USER=dict(cast=str, default='postgres'),
    POSTGRES_PASSWORD=dict(cast=str, default='postgres'),
    POSTGRES_HOST=dict(cast=str, default='postgres'),
    POSTGRES_PORT=dict(cast=int, default=5432),
)


class PostCreateView(View):

    async def post(self):
        try:
            data = await self.request.json()
        except ValueError:
            return Response(status=400)

        pool = self.request.app['postgres_pool']
        async with pool.acquire() as connection:
            await connection.execute("""
                INSERT INTO core_blogpost(text, title, published) VALUES($1, $2, $3)
            """, data['text'], data['title'], datetime.now())

        return Response(status=201)



async def app_init():
    app = Application()

    db_str = "postgresql://{USERNAME}:{PASSWORD}@{HOST}:{PORT}/{DB_NAME}"
    db_settings = db_str.format(
        USERNAME=env('POSTGRES_USER'),
        PASSWORD=env('POSTGRES_PASSWORD'),
        HOST=env('POSTGRES_HOST'),
        PORT=env('POSTGRES_PORT'),
        DB_NAME=env('POSTGRES_DB'),
    )
    app['postgres_pool'] = await asyncpg.create_pool(db_settings)

    return app


def create_app():
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(app_init())

    app.router.add_route('POST', '/api/posts/', PostCreateView)

    return app


app = create_app()


if __name__ == '__main__':
    from aiohttp.web import run_app

    run_app(app, host='0.0.0.0', port=8001)
