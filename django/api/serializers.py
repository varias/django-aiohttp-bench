from rest_framework import serializers

from core.models import BlogPost


class BlogPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogPost
        fields = ('id', 'text', 'title', 'published')
        read_only_fields = ('id', 'published')
