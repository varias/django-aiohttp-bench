from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView

from core.models import BlogPost

from .serializers import BlogPostSerializer
from .pagination import BlogPostPagination


class BlogPostCreateView(CreateAPIView):
    serializer_class = BlogPostSerializer


class BlogPostListView(ListAPIView):
    serializer_class = BlogPostSerializer
    pagination_class = BlogPostPagination
    queryset = BlogPost.objects.all().order_by('published')


class BlogPostRetrieveView(RetrieveAPIView):
    serializer_class = BlogPostSerializer
    queryset = BlogPost.objects.all()
