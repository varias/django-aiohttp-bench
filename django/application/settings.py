from os.path import abspath, dirname, join

from envparse import Env

env = Env(
    DEBUG=dict(cast=bool, default=False),
    ALLOWED_HOSTS=dict(cast=list, subcast=str),
    STATIC_ROOT=dict(cast=str, default='/var/www/static'),
    MEDIA_ROOT=dict(cast=str, default='/var/www/media'),
    POSTGRES_DB=dict(cast=str, default='application'),
    POSTGRES_USER=dict(cast=str, default='postgres'),
    POSTGRES_PASSWORD=dict(cast=str, default='postgres'),
    POSTGRES_HOST=dict(cast=str, default='postgres'),
    POSTGRES_PORT=dict(cast=int, default=5432),
    SECRET_KEY=dict(cast=str, default='')
)

BASE_DIR = dirname(dirname(abspath(__name__)))

SECRET_KEY = env('SECRET_KEY')

DEBUG = env('DEBUG')

ALLOWED_HOSTS = env('ALLOWED_HOSTS')

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.staticfiles',

    'core',
    'api',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'application.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'application.wsgi.application'

DATABASES = {
    'default': {
        # 'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'ENGINE': 'django_db_geventpool.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_DB'),
        'USER': env('POSTGRES_USER'),
        'PASSWORD': env('POSTGRES_PASSWORD'),
        'HOST': env('POSTGRES_HOST'),
        'PORT': env('POSTGRES_PORT'),
        'ATOMIC_REQUESTS': False,
        'CONN_MAX_AGE': 0,
        'OPTIONS': {
            'MAX_CONNS': 10
        }
    }
}

AUTH_PASSWORD_VALIDATORS = []

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = env('STATIC_ROOT')

MEDIA_URL = '/media/'
MEDIA_ROOT = env('MEDIA_ROOT')

LOG_LEVEL = 'DEBUG' if DEBUG else 'WARNING'
LOG_DIR = '/var/log/application'
LOGGING = {
    'version': 1,
    'handlers': {
        'requests': {
            'level': LOG_LEVEL,
            'class': 'logging.FileHandler',
            'filename': join(LOG_DIR, 'requests.log'),
        },
        'security': {
            'level': LOG_LEVEL,
            'class': 'logging.FileHandler',
            'filename': join(LOG_DIR, 'security.log'),
        },

    },
    'loggers': {
        'django.request': {
            'handlers': ['requests'],
            'level': LOG_LEVEL,
            'propagate': True,
        },
        'django.security': {
            'handlers': ['security'],
            'level': LOG_LEVEL,
            'propagate': True,
        },
    },
}
